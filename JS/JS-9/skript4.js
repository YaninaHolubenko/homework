//Створіть слайдер, який кожні 3 сек змінюватиме зображення 

const div = document.createElement("div");
div.classList.add("container");
document.body.append(div);

const image = document.createElement("img");
image.setAttribute("src", "img/1.jpg")
div.append(image);

let imgArr = [
    "img/1.jpg",
    "img/2.jpg",
    "img/3.jpg",
    "img/4.jpg",
    "img/5.jpg",
    "img/6.jpg",
    "img/7.jpg",
];
let imageIndex = 0;
const slide = () => {
    image.setAttribute('src', imgArr[imageIndex]);
    imageIndex++;
    if (imageIndex == imgArr.length) imageIndex = 0;
}
slide(setInterval(slide, 3000))
