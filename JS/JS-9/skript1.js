// Створіть на сторінці div і дайте йому зовнішній відступ 150 пікселів. 
// Використовуючи JS виведіть у консоль відступ

const div = document.createElement("div");
const body = document.querySelector('body');
const first = body.firstChild;
body.insertBefore(div,first );


div.style.margin = "150px";
div.style.border = "2px red solid";
div.style.backgroundColor = 'red';

const p = document.createElement("p");
div.append(p);
p.style.color = 'white';
p.style.textAlign = 'center';
p.innerText = `Зовнішній відступ div = ${div.style.margin}`;

console.log(`Зовнішній відступ div = ${div.style.margin}`)



