/**
* Реалізуйте програму перевірки телефону
* Використовуючи JS Створіть поле для введення телефону та кнопку збереження
* Користувач повинен ввести номер телефону у форматі 000-000-00-00 
* Після того як користувач натискає кнопку зберегти перевірте правильність введення номера,
* якщо номер правильний зробіть зелене тло і використовуючи document.location 
* переведіть користувача на сторінку https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg 
* якщо буде помилка, відобразіть її в діві до input.
*/


//container
const body = document.querySelector('body');
const container = document.createElement("div");
container.classList.add('container');
const first = body.firstChild;
body.insertBefore(container, first);

//span
const span = document.createElement('span');
span.classList.add('span');
span.innerHTML = 'Введіть номер телефону у форматі 000-000-00-00 ';
container.append(span);

//input
const input = document.createElement('input');
input.classList.add('input');
container.append(input);
input.setAttribute("id", "phone")
input.setAttribute("type", "text");
input.setAttribute("placeholder", "000-000-00-00");

//button
const button= document.createElement('button');
button.classList.add('button');
button.innerHTML = 'Зберегти';
container.append(button);

//error
const error = document.createElement('div');
error.classList.add('error');
container.insertBefore(error,input);

const save = () => {
    const number = document.querySelector('.input').value;
    const pattern = /^(\d{3}\d{3}\d{2}\d{2})*$/;

    if(number.match(pattern)) {
        alert('Номер збережено');
        body.style.backgroundColor = 'green';
        document.location.replace('https://uk.wikipedia.org/wiki/%D0%A3%D0%BA%D1%80%D0%B0%D1%97%D0%BD%D0%B0_%D0%BF%D0%BE%D0%BD%D0%B0%D0%B4_%D1%83%D1%81%D0%B5!')
    } else {
        alert('Номер введено у невірному форматі');
        document.querySelector('.error').innerText = "Неправильний формат телефону!";
        input.style.marginTop = '5px';
    }
}

button.onclick = save;
