/*
    Створіть програму секундомір. 
    Секундомір матиме 3 кнопки "Старт, Стоп, Скидання"
    При натисканні на кнопку стоп фон секундоміра має бути червоним, 
        старт - зелений, 
        скидання - сірий * 
    Виведення лічильників у форматі ЧЧ:ММ:СС
    Реалізуйте Завдання використовуючи синтаксис ES6 та стрілочні функції
*/
const container = document.querySelector('.container');
const hours = document.getElementById('hours');
const minutes = document.getElementById('minutes');
const seconds = document.getElementById('seconds');

const start = document.getElementById('start');
const stopButton = document.getElementById('stop');
const reset = document.getElementById('reset');


let hoursCounter = 0;
let minutesCounter = 0;
let secondsCounter = 0;

let intervalHandler;

const tick = () => {
    if (secondsCounter >= 60) {
        secondsCounter = 0;
        minutesCounter++;
        if (minutesCounter >= 60) {
            minutesCounter = 0;
            hoursCounter++;
        }
    }
    secondsCounter++;
}

const newTime = () => {
    seconds.innerText = secondsCounter < 10 ? ("0" + secondsCounter) : secondsCounter;
    minutes.innerText = minutesCounter < 10 ? ("0" + minutesCounter) : minutesCounter;
    hours.innerText = hoursCounter < 10 ? ("0" + hoursCounter) : hoursCounter;
}

const startTimer = () => {
    start.style.background = "none";
    start.style.backgroundColor = "green";
    reset.style.background = "linear-gradient(45deg, #7901cf, #3171fb)";
    stopButton.style.background = "linear-gradient(45deg, #7901cf, #3171fb)";
    container.style.backgroundColor= "green";
    intervalHandler = setInterval(() => {
        tick();
        newTime();
    }, 1000)

}
const stopTimer = () => {
    clearInterval(intervalHandler);
    stopButton.style.background = "none";
    stopButton.style.backgroundColor = "red";
    start.style.background = "linear-gradient(45deg, #7901cf, #3171fb)";
    reset.style.background = "linear-gradient(45deg, #7901cf, #3171fb)";
    container.style.backgroundColor= "red";
}

const resetTimer = () => {
    clearInterval(intervalHandler);
    reset.style.background = "none";
    reset.style.backgroundColor = "silver";
    stopButton.style.background = "linear-gradient(45deg, #7901cf, #3171fb)";
    start.style.background = "linear-gradient(45deg, #7901cf, #3171fb)";
    container.style.backgroundColor= "silver";
    hours.innerHTML = "00";
    minutes.innerHTML = "00";
    seconds.innerHTML = "00";
    hoursCounter = 0;
    minutesCounter = 0;
    secondsCounter = 0;
}


start.onclick = startTimer;
stopButton.onclick = stopTimer;
reset.onclick = resetTimer;

