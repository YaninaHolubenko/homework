//Домашнє завдання до уроку 5

/*Задание: Создать объект “Документ”, в котором определить свойства 
“Заголовок, тело, футер, дата”. Создать в объекте вложенный объект - “Приложение”. Создать в объекте “Приложение”, 
вложенные объекты, “Заголовок, тело, футер, дата”. Создать методы для заполнения и отображения документа.*/


var documentForApp = {
    header : "",
    body : "",
    footer : "",
    data : "",
    application :{
            header : {},
            body : {},
            footer : {},
            data : {},
    },
    fill : function () {
        this.header = prompt("Введіть назву для header", "header");
        this.body = prompt("Введіть назву body", "body");
        this.footer = prompt("Введіть назву footer", "footer");
        this.data = prompt("Введіть дату створення", "data");
        this.application.header = prompt("Введіть назву вашого додатку", "application header");
        this.application.body = prompt("Введіть назву body вашого додатку", "application body");
        this.application.footer = prompt("Введіть назву footer вашого додатку", "application footer");
        this.application.data = prompt("Введіть дату створення вашого додатку", "application data");

    },
    show : function () {
        document.write(`<p><b> Назва для header: ${this.header}</b><p>`);
        document.write(`<p> Назва body: ${this.body}</p>`);
        document.write(`<p> Назва footer: ${this.footer}</p>`);
        document.write(`<p> Дата створення: ${this.data}</p>`);
        document.write(`<p> <b> Назва вашого додатку: ${this.application.header} </b><p>`);
        document.write(`<p> Назва body вашого додатку: ${this.application.body}</p>`);
        document.write(`<p> Назва  footer вашого додатку: ${this.application.footer}</p>`);
        document.write(`<p> Дата створення вашого додатку: ${this.application.data}</p></div>`);
    }
};

documentForApp.fill();
documentForApp.show();





