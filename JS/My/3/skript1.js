/*1) let stringNums = ['1', '34', 5, 7, '8', '10']; 
Створити функцію яка повертає массив в якому всі значення типу Number*/


let stringNums = ['1', '34', 5, 7, '8', '10'];

function b(i){
    return Number(i)
}

function name (array,callbackFunction){
    let newArr= [];
    for (let i = 0; i < array.length; i++){
    newArr.push(callbackFunction(array[i]))
    }
    console.log(newArr);
    return newArr
}

name(stringNums,b)

/*
//Другий спосіб

let newArray = Array.from(stringNums, Number);  
console.log(newArray)*/ 
