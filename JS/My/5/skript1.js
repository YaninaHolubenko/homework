/*1)Написати регулярку яка вибиарає всі цифри без знаків (, + , ) і повертає строкою
PhoneNumber = "+38 (063) 223-23-23";
Створити форму в якій є інпут і кнопка з надписом перевірити номер.
Після натискання на кнопку номер з інпуту повинен бути перевірений регуляркою і виведено в модалці номер вірний чи не вірний.*/

//container
const body = document.querySelector('body');
const container = document.createElement("div");
container.classList.add('container');
const first = body.firstChild;
body.insertBefore(container, first);


//span
const span = document.createElement('span');
span.classList.add('span');
span.innerHTML = 'Введіть номер телефону у форматі +38 (063) 223-23-23'; //Як зробити щоб знаки вводилися автоматично?
container.append(span);

//input
const input = document.createElement('input');
input.classList.add('input');
container.append(input);
input.setAttribute("id", "phone")
input.setAttribute("type", "tel");
input.setAttribute("placeholder", "+38 (063) 223-23-23");

//button
const button= document.createElement('button');
button.classList.add('button');
button.innerHTML = 'Перевірити';
container.append(button);

const save = () => {
    const number = document.querySelector('.input').value;
    const pattern = /\+38 \(\d{3}\) \d{3}-\d{2}-\d{2}/;//Шаблон для перевірки введеного номеру

    if(number.match(pattern)) {//Перевірка на відповідність
        console.log(number);
        alert('Номер збережено');
        let result = number.replace(/[^0-9]/g,'');//Виводимо строку чисел без знаків
        console.log(`Cтрока чисел без знаків ${result}`);
    } else {
        alert('Номер введено у невірному форматі');
    }
    let myArr = [];

}

button.onclick = save;





   