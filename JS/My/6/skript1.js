/*1) Створити слайдер 2 в 1 ( щоб картинки змінювались кожні 3 секунди і були кнопки попередня картинка і наступна)const 
div = document.createElement("div");*/
window.onload = () => {
    const body = document.querySelector('body');     
    let wrapper = document.createElement('div');
    wrapper.classList.add('wrapper');
    body.appendChild(wrapper);
    
       
    let wrapperForImg = document.createElement('div');
    wrapperForImg.classList.add('wrapperForImg');
    wrapper.appendChild(wrapperForImg);
    
    let buttonNext = document.createElement('button');
    buttonNext.innerHTML = 'Next';
    buttonNext.classList.add('buttonNext');
    wrapper.appendChild(buttonNext);
    
    let buttonPrevious = document.createElement('button');
    buttonPrevious.innerHTML = 'Previous';
    buttonPrevious.classList.add('buttonPrevious');
    wrapper.appendChild(buttonPrevious);
    
    
    const image = document.createElement("img");
    image.setAttribute("src", "img/1.jpg")
    wrapperForImg.append(image);
    
    let imgArr = [
        "img/1.jpg",
        "img/2.jpg",
        "img/3.jpg",
        "img/4.jpg",
        "img/5.jpg",
        "img/6.jpg",
    ];
    
    
    let imageIndex = 0;
    
    buttonPrevious.addEventListener('click', () => {
        --imageIndex;
        if (imageIndex < 0) imageIndex = imgArr.length - 1;
        image.setAttribute('src', imgArr[imageIndex]);
    }
    );
    
    buttonNext.addEventListener('click', () => {
        ++imageIndex;//Чому при першому натисканні пропускає один індекс?
        if (imageIndex == imgArr.length) imageIndex = '0';
        image.setAttribute('src', imgArr[imageIndex]);
    }
    );

    const slide = () => {
    image.setAttribute('src', imgArr[imageIndex]);
    ++imageIndex;
    if (imageIndex == imgArr.length) imageIndex = 0;
    }
    

    wrapper.addEventListener('mouseenter',(event)=>{//чому спрацьовує тільки один раз, а при повторному наведенні нічого не відбувається?
        clearInterval(1);//clearInterval(slide) нічого не відбувається
    });
   /*  wrapper.addEventListener('mouseout',(event)=>{ //якщо розкоментувати цей блок, то слайдер дуріє
        slide(setInterval(slide,3000));
    }); */
    
    slide(setInterval(slide, 3000));
    }
    
    
