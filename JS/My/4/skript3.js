/* Завдання 2 на використання html + css і js
 Створити кнопку, по натисканню на кнопку повино зявлятись модальне вікно, в модалці повинна бути кнопка закрити вікно. 
 Під модальним вікном повиний бути блок підкладка при кліку на який модальне вікно закривається 


<button #openbutton class="openButton">Відкрити модальне вікно</button>
<div #modalWindow class="modal">
    <div class="content">
        <div class="text">Hello world!</div>
        <button class="closeButton1">Close</button>
    </div>
    <button class="closeButton2">Close</button>
</div>*/


let modal = document.getElementById('modalWindow');
console.log(modal)
let btn = document.getElementById('openButton');
console.log(btn)
let btn1 = document.querySelector('.closeButton1');
console.log(btn1)
let btn2 = document.querySelector('.closeButton2');

btn.onclick = function(){
    modal.style.display = "block";
    modal.classList.toggle('show');
}
 
btn1.onclick = function(){
    modal.style.display = "none";
    modal.classList.toggle('show')
}
btn2.onclick = function(){
    modal.style.display = "none";
    modal.classList.toggle('show')
}
window.onclick = function(event){//якщо користувач натисне просто на саме модальне вікно, то воно також закриється
if (event.target == modal){
   modal.style.display = "none";
   
}
}


/* .classList.toggle('show');
.classList.contains('warning'); */