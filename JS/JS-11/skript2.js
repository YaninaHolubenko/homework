/*Задание 2 Создайте страницу с несколькими блоками текста. 
Реализуйте обработчики событий таким образом, чтобы при нажатии на кнопку r - 
текст становился красного цвета, кнопка g сделает цвет текста зеленым, а b-СИНИМ. 
*/


let redBtn = document.getElementById('red');
let greenBtn = document.getElementById('green');
let blueBtn = document.getElementById('blue');

let textElements = document.querySelectorAll('p');

document.addEventListener('keydown', pressing);

function pressing(e) {
    if(e.code === "KeyR") {
        textElements.forEach(item=>{
            item.style.color = 'red';
        })
    };
    if(e.code === "KeyG") {
        textElements.forEach(item=>{
            item.style.color = 'green';
        })
    };
    if(e.code === "KeyB") {
        textElements.forEach(item=>{
            item.style.color = 'blue';
        })
    };
};