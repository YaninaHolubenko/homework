/*Задание 4 Разработайте страницу, которая будет выводить сообщение
 «сохранено» при нажатии на клавиши Ctr1 + S, 
«выбрано все» при нажатии на Ctr1 + А и 
«сохранено все» при нажатии на комбинацию Ctr1 + Shift + S.*/

document.documentElement.addEventListener("keydown",pressingComb);
  

function pressingComb(e) {
    if (e.code== "KeyS" && e.ctrlKey && e.shiftKey===false) {
      e.preventDefault();
      alert('Сохранено')
    };
    if (e.code== "KeyA" && e.ctrlKey) {
      e.preventDefault();
      alert('Выбрано всё')
    };
    if (e.code== "KeyS" && e.ctrlKey && e.shiftKey) {
      e.preventDefault();
      alert('Сохранено всё')
    }
  }
