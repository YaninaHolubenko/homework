/*Задание 3 Сделайте кнопку с надписью «получить скидку». 
При наведение кнопка должна «убегать» от курсора не давая пользователю нажать себя. 
*/

const saleBtn = document.querySelector(".saleBtn");
  
//Функція random
const random = (min, max) => {
    console.log(max)
    const rand = min + Math.random() * (max - min + 1);
    return Math.floor(rand);
}
// Обробник подій
saleBtn.addEventListener('mouseenter', () => {
    saleBtn.style.left = `${random(0, 80)}%`;
    saleBtn.style.top = `${random(0, 90)}%`;
});

// У разі перемоги
saleBtn.addEventListener('click', () => {
    alert('Congrats! You hit the button! What are you doing with your life? :D');
});


