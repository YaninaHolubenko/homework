/* №2 Реализовать функцию для создания объекта "пользователь".
    Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser. 
    При вызове функция должна спросить у вызывающего имя и фамилию. Используя данные, введенные 
    пользователем, создать объект newUser со свойствами firstName и lastName. Добавить в объект 
    newUser метод getLogin(), который будет возвращать первую букву имени пользователя, соединенную 
    с фамилией пользователя, все в нижнем регистре (например, Ivan Kravchenko → ikravchenko). 
    Создать пользователя с помощью функции createNewUser(). Вызвать у пользователя функцию getLogin(). 
    Вывести в консоль результат выполнения функции.*/

    
//Створюємо функцію (class) createNewUser.
class createNewUser {
    constructor (firstName, lastName){
      this.firstName = firstName;
      this.lastName = lastName;
    }
    
    // Створюємо метод getLogin() - повертаємо першу літеру ім'я користувача, об'єднану з прізвищем користувача у нижньому регістрі.
    getLogin(){
      return this.firstName.substr(0, 1).toLowerCase() + this.lastName.toLowerCase();
    }
  }
  
  //Викликаємо функцію (class) createNewUser та через модальне вікно користувач вводить ім'я та прізвище, 
  //створюємо об'єкт екземпляру(екземпляр) NewUser.
  let NewUser = new createNewUser(prompt("Ваше ім'я", "Ім'я"), prompt("Ваше прізвище", "Прізвище"));
  
  //Виводимо в консоль результат виконання функції.
  console.log(NewUser.getLogin())
  
  