
    /*№3 Дополнить функцию createNewUser() методами подсчета возраста пользователя и его паролем.
    Возьмите выполненное задание выше (созданная вами функция createNewUser()) и дополните ее 
    следующим функционалом: При вызове функция должна спросить у вызывающего дату рождения (текст 
    в формате dd.mm.yyyy) и сохранить ее в поле birthday. Создать метод getAge() который будет 
    возвращать сколько пользователю лет. Создать метод getPassword(), который будет возвращать первую 
    букву имени пользователя в верхнем регистре, соединенную с фамилией (в нижнем регистре) и годом 
    рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992). Вывести в консоль результат 
    работы функции createNewUser(), а также функций getAge() и getPassword() созданного объекта.*/


  
//Створюємо функцию (class) createNewUser.
class createNewUser {
  constructor (firstName, lastName, birthDay){
    this.firstName = firstName;
    this.lastName = lastName;
    this.birthDay = birthDay;
  }
  
  /*Створюємо метод getLogin() з логікою - повернути першу літеру ім'я користувача, конкатинація з прізвіщем користувача. 
  Усе в нижньому регістрі.*/
  getLogin(){
    return this.firstName.substr(0, 1).toLowerCase() + this.lastName.toLowerCase();
  }

  // Створюємо метод getAge() з логікою - підрахунок віку користувача. 
  getAge() {
    return ((new Date().getTime() - new Date(this.birthDay.reverse()).getTime()) / (24 * 3600 * 365.25 * 1000)) | 0;
  }  

  //Створюємо метод getPassword() з логікою - повертати першу літеру ім'я користувача в верхньому регістрі, конкатинація з прізвищем (у нижньому регістрі) та роком народження.
  getPassword(){
    return this.firstName.substr(0, 1).toUpperCase() + this.lastName.toLowerCase() + this.birthDay[0];
  }
}

/*Викликаємо функцію (class) createNewUser та через модальне вікно отримуємо інформацію від користувача (ім'я, прізвище, дата народження), 
та створюємо об'ект(екземпляр) NewUser.*/
let NewUser = new createNewUser(prompt("Введите имя", "Ivan"), prompt("Введите фамилию", "Ivanov"), prompt("Введите дату рождения фамилию", "dd.mm.yyyy").split('.'));

//Виводимо в консоль результат виконання коду.
console.log(NewUser.getLogin());
console.log(NewUser.getAge());
console.log(NewUser.getPassword());5
