/*Сделайте функцию, которая считает и выводит количество своих вызовов.
                    func(); //выведет 1
                    func(); //выведет 2
                    func(); //выведет 3
                    func(); //выведет 4*/

function count() {
    let i = 1;
    return () => ("Кількість вікликів функції: " + (i++));
    }
                    
let show = count();
                    
console.log(show());
console.log(show());
console.log(show());