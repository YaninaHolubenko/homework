/*Создайте функцию user, сделайте так чтобы функция выводила в консоль контекст

Создайте обьект userOne, добавьте к нему свойства имя, фамилия, возраст и функцию, которая будет выводить  Привет! Я имя + фамилия  Выведите имя и фамилию пользователя с объекта userOne.

Сделайте так, чтобы контекст в методе объекта userOne был глобальный объект window.

Создайте еще один объект userTwo и заполните его теми же свойствами, но без метода.

Выведите информацию с объекта userTwo используя метод объекта userOne.

Работа с контекстом(bind call apply) bind call apply*/

const userOne = {
    name : "Anne",
    lastName : "Smith",
    age : 25,

    user() {
        console.log(`Привет! Я ${this.name} ${this.lastName}`);
    }
}

const userTwo = {
    name : "Nick",
    lastName : "Olho",
    age : 28,

}
userOne.user();
userOne.user.bind(userTwo)();
