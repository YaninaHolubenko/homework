/*Сделайте функцию, каждый вызов который будет генерировать случайные числа от 1 до 100, 
но так, чтобы они не повторялись, пока не будут перебраны все числа из этого промежутка.
Решите задачу через замыкания - в замыкании должен хранится массив чисел, которые уже были сгенерированы функцией.*/

let arr = [];

function generateNum() {
    return(function (){
        let num = Math.floor(Math.random() * 100);
        if (arr.indexOf(num) === -1) {
           arr.push(num) 
           console.log(arr)
        }
    })
   
}
let result = generateNum();
result()
result()
result()
result()
result()
result()
result()
result()
