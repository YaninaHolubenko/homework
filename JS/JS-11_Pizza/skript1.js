/* Создать сайт по заказу пиццы используя семантическую верстку. (Пример <a href="https://dominos.ua/ru/kyiv/create/"
 style="cursor: pointer;">https://dominos.ua/ru/kyiv/create/</a>  ) <br>
На сайте должна быть возможность сделать пиццу самому. <br>
 При создании пиццы пользователь выбирает ингредиенты и размер пиццы. ( маленькая, средняя или большая). 
Ингредиенты ( сыр, пепперони, перец, кукуруза, ананасы и т.д) и также 3 вида соуса. При создании пиццы цена увеличивается.
 Все ингредиенты и коржи должны быть в виде картинок и накладываются друг на друга ( https://ru.wikipedia.org/wiki/Drag-and-drop ) 
 при выборе в конце принять от пользователя телефон, email, имя и проверить на правильность введения. 
 После чего весь результат заказа отправить на почту philipsevene@gmail.com.
<br><br>Так же создайте банер получить скидку на пиццу 30% и сделайте чтобы банер от курсора пользователя убегал.</li> */


//Функція реклами
const saleBtn = document.querySelector(".saleBtn");
  
//Функція random
const random = (min, max) => {
    const rand = min + Math.random() * (max - min + 1);
    return Math.floor(rand);
}
// Обробник подій
saleBtn.addEventListener('mouseenter', () => {
    saleBtn.style.left = `${random(0, 80)}%`;
    saleBtn.style.top = `${random(0, 70)}%`;
});

// У разі перемоги
saleBtn.addEventListener('click', () => {
    
});


const pizzaSize = document.querySelectorAll('input[name=size]');

const dragElements = document.querySelectorAll('.item_menue img');

const elementsResult = document.querySelectorAll('.item');

const btnsDelete = document.querySelectorAll('.button_delete');

const pizza_result = document.querySelector('.pizza_result');

const totalSpan = document.querySelector('.h2');

const orderBtn = document.getElementById('button_order');


//Модальне вікно
const modalWindow = document.querySelector('.modal_window');

const modalWindowTotalCost = document.querySelector('#form_order_cost');

const modalWindowOrderDetails = document.querySelector('#form_order_details');

const modalWindowSubmitBtn = document.getElementById('form_buttonRegister');




//Функція перетягування інгрідієнтів
const addIngredients = ingredients=>{
const ingredientsArray = Array.from(ingredients);
const elementsResultArray = Array.from(elementsResult);

for (let node of ingredients){

    node.addEventListener('dragend', event=>{
        const index = ingredientsArray.indexOf(event.target);
        if(elementsResultArray[index].classList.contains('item_Display_active')){
            alert('This ingredient has already been added');
            return
        }else{
            elementsResultArray[index].classList.toggle('item_Display_active');
            calculatePrice();
    }
        
    });
};
};
//Запуск функції перетягування інгрідієнтів
addIngredients(dragElements);

//Вибір розміру піци
const checkedSizeTotal = ()=>{
    const pizzaSizeArray = Array.from(pizzaSize);
    for (let node of pizzaSizeArray){
        node.addEventListener('click', event=>{
            const sizeTotal = document.querySelector('input[name=size]:checked').value;
            const sizeTotalPrize = document.querySelector('input[name=size]:checked').nextElementSibling.textContent;
            calculatePrice();
        })
   
}}
//функція видалення інгрідієнтів
const deleteItems = ()=>{
    const btnsDeleteArray = Array.from(btnsDelete);
    for (let node of btnsDeleteArray){
        node.addEventListener('click', event=>{
            const index = btnsDeleteArray.indexOf(event.target);
            const elementsResultArray = Array.from(elementsResult);
            elementsResultArray[index].classList.remove('item_Display_active');
            calculatePrice()
        })  
}
};
deleteItems()

checkedSizeTotal()

//Функція для підрахунку вартості замовлення
function calculatePrice() {
    const ingredientsTotal = document.querySelectorAll('.item_Display_active img');
    const sizeTotal = document.querySelector('input[name=size]:checked').value;
    let sum = 0;
    ingredientsTotal.forEach((item) => {
        sum += Number(item.title);
    })
    form_order_cost.value = `Total: ${Number(sum)+Number(sizeTotal)}$`;
    totalSpan.innerHTML = form_order_cost.value
}
calculatePrice();

function openModalWindow(){
    orderBtn.addEventListener('click', event=>{
        const ingredientsTotal = document.querySelectorAll('.item_Display_active img');
        if(ingredientsTotal.length<3){
            alert('Not enough ingredients to order. Choose at least three ingredients');
            return;
        }else{
            let ingredientsTotalArray = Array.from(ingredientsTotal)
            let newArr = [];
            function getTotalIngredients(arr){
               
            for (let i = 0; i < arr.length; i++){
                newArr.push(arr[i].alt);
            }
            return newArr};

            getTotalIngredients(ingredientsTotalArray);
            modalWindow.classList.toggle('item_Display_active');
            totalSpan.innerHTML = form_order_cost.value;
            const sizeTotal = document.querySelector('input[name=size]:checked').nextElementSibling.title;
            let totalIngredients = `Your order is a ${sizeTotal} pizza with ${newArr}`;
            modalWindowOrderDetails.innerHTML = totalIngredients;}
        })  
}
openModalWindow()

//Валідація форми

let title = document.querySelector('.form_title');
let inputName = "";  
let inputEmail = ""; 
let inputPhone = ""; 

let buttonConfirm = document.getElementById('form_buttonRegister');
let formContainer = document.querySelector('.form_container');
let textArea = document.querySelector('textarea');
let inputs = document.querySelectorAll("input");

function save (e) {
    e.preventDefault();//Не розібралася як відправити дані з форми на пошту?
    inputNameContainer = document.getElementById('form_name');
    inputEmailContainer = document.getElementById('form_email');
    inputPhoneContainer = document.getElementById('form_phone');
    inputName = document.getElementById('form_name').value;
    inputEmail = document.getElementById('form_email').value;
    inputPhone = document.getElementById('form_phone').value;

    const patternName = /[a-zа-яёїієґъ'-]+[a-zа-яёїієґъ'-]+[a-zа-яёїієґъ'-]+/i;
    const patternEmail = /\b[a-z0-9._]+@[a-z0-9.-]+\.[a-z]{2,4}\b/i;
    const patternPhone = /\+\d{2}\d{3}\d{3}\d{2}\d{2}/ 

    if(inputName.match(patternName)) {
        
    } else {
        inputNameContainer.classList.toggle('error');
        alert("An incomplete name was entered, or the name was entered incorrectly"); 
        return
    }
    if(inputEmail.match(patternEmail)) {
    } else {
        inputEmailContainer.classList.toggle('error');
        alert("The email was entered incorrectly");
        return
    }
    if(inputPhone.match(patternPhone)) {
    } else {
        inputPhoneContainer.classList.toggle('error');
        alert("The phone number was entered incorrectly");
        return
    }
    
    if(
        inputName.match(patternName),
        inputEmail.match(patternEmail),
        inputPhone.match(patternPhone)
        )
        {
        inputs.forEach(item=>{
            item.classList.toggle('hide');
            })
            textArea.classList.toggle('hide');
            title.textContent = "Thanks for your order";
            buttonConfirm.textContent = 'Close';
            buttonConfirm.addEventListener('click', function (event){
                //Як сховати елементи з піци?
                /* const ingredientsTotal = document.querySelectorAll('.item_Display_active img'); 
               
                ingredientsTotal.forEach(element =>{
                    element.classList.remove('item_Display_active')
                }) */
                formContainer.classList.toggle('hide');
                
            })
        }
}
buttonConfirm.addEventListener('click', function(event){
    save(event)
});


//slider
let $img = $("#slider img"),
i = 1;
isSlide = false;

$(`#slider img:not(:nth-child(1))`).css({left:`${$("#slider").css("width")}`})
// Вперед
function getNextImg() {
if (!isSlide) {
    isSlide = true;
    let $width = $("#slider").css("width");
    $(`#slider img:not(:nth-child(${i}))`).css({ left: `${$width}` });
    $(`#slider img:nth-child(${i})`).animate({ left: `-${$width}` }, 800);
    if (i === $img.length) {
        i = 1;
        $(`#slider img:nth-child(1)`).animate({ left: "0" }, 800, () => { isSlide = false; });
    }
    else {
        $(`#slider img:nth-child(${i + 1})`).animate({ left: "0" }, 800, () => { isSlide = false; });
        i++
    };
}
}
// Назад
function getPrevImg() {
if (!isSlide) {
    isSlide = true;
    let $width = $("#slider").css("width");
    $(`#slider img:not(:nth-child(${i}))`).css({ left: `-${$width}` })
    $(`#slider img:nth-child(${i})`).animate({ left: `${$width}` }, 800);
    if (i === 1) {
        i = $img.length;
        $(`#slider img:nth-child(3)`).animate({ left: "0" }, 800, () => { isSlide = false; });
    }
    else {
        $(`#slider img:nth-child(${i - 1})`).animate({ left: "0" }, 800, () => { isSlide = false; });
        i--
    };
}
}

$("#prev").click(getPrevImg);
$("#next").click(getNextImg);
setInterval(getNextImg, 15000);









