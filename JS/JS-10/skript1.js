/**Задание 1*

## Задание 

Реализовать слайдер на чистом Javascript.

#### Технические требования:
- Создать HTML разметку, содержащую кнопки `Previous`, `Next`, и картинки (6 штук), которые будут пролистываться горизонтально. 
- На странице должна быть видна только одна картинка. Слева от нее будет кнопка `Previous`, справа - `Next`.
- По нажатию на кнопку `Next` - должна появиться новая картинка, следующая из списка.
- По нажатию на кнопку `Previous` - должна появиться предыдущая картинка.
- Слайдер должен быть бесконечным, т.е. если в начале нажать на кнопку `Previous`, то покажется последняя картинка, а если нажать на `Next`, когда видимая - последняя картинка, то будет видна первая картинка.
- Пример работы слайдера можно увидеть(http://kenwheeler.github.io/slick/) (первый пример).*/

window.onload = () => {
const body = document.querySelector('body');     
let wrapper = document.createElement('div');
wrapper.classList.add('wrapper');
body.appendChild(wrapper);

   
let wrapperForImg = document.createElement('div');
wrapperForImg.classList.add('wrapperForImg');
wrapper.appendChild(wrapperForImg);

let buttonNext = document.createElement('button');
buttonNext.innerHTML = 'Next';
buttonNext.classList.add('buttonNext');
wrapper.appendChild(buttonNext);

let buttonPrevious = document.createElement('button');
buttonPrevious.innerHTML = 'Previous';
buttonPrevious.classList.add('buttonPrevious');
wrapper.appendChild(buttonPrevious);


const image = document.createElement("img");
image.setAttribute("src", "img/1.jpg")
wrapperForImg.append(image);

let imgArr = [
    "img/1.jpg",
    "img/2.jpg",
    "img/3.jpg",
    "img/4.jpg",
    "img/5.jpg",
    "img/6.jpg",
];


let imageIndex = 0;

buttonPrevious.addEventListener('click', () => {
    imageIndex--;
    if (imageIndex < 0) imageIndex = imgArr.length - 1;
    image.setAttribute('src', imgArr[imageIndex]);
}
);

buttonNext.addEventListener('click', () => {
    imageIndex++;
    if (imageIndex == imgArr.length) imageIndex = '0';
    image.setAttribute('src', imgArr[imageIndex]);
}
);
}

