/*## Задание

Написать реализацию функции, которая позволит создавать объекты типа Hamburger, используя возможности стандарта ES5.

#### Технические требования:
- Некая сеть фастфудов предлагает два вида гамбургеров:

- маленький (50 гривен, 20 калорий)
- большой (100 гривен, 40 калорий)
- Гамбургер должен включать одну дополнительную начинку (обязательно):

- сыр (+ 10 гривен, + 20 калорий)
- салат (+ 20 гривен, + 5 калорий)
- картофель (+ 15 гривен, + 10 калорий)
- Дополнительно, в гамбургер можно добавить приправу (+ 15 гривен, 0 калорий) и полить майонезом (+ 20 гривен, + 5 калорий).
- Необходимо написать программу, рассчитывающую стоимость и калорийность гамбургера. 
Обязательно нужно использовать ООП подход (подсказка: нужен класс Гамбургер, константы, методы для выбора опций и рассчета нужных величин).
- Код необходимо написать под стандарт ES5.
- Код должен быть защищен от ошибок. Представьте, что вашим классом будет пользоваться другой программист. 
Если он передает неправильный тип гамбургера, например, или неправильный вид добавки, 
должно выбрасываться исключение (ошибка не должна молча игнорироваться). https://learn.javascript.ru/try-catch
- Написанный класс должен соответствовать следующему jsDoc описанию 
(то есть содержать указанные методы, которые принимают и возвращают данные указанного типа и выбрасывают исключения указанного типа. 
Комментарии ниже тоже можно скопировать в свой код):*/

const pPrice = document.querySelector(".price");
const pCalories = document.querySelector(".calories");
const pToppings = document.querySelector(".toppings");
const checkboxMayo = document.querySelector('#topping_mayo');
const checkboxSpice = document.querySelector('#topping_spice');
const pGetSize = document.querySelector(".size");
const pGetStuffing = document.querySelector(".stuffing");

class Hamburger {
    constructor(size, stuffing) {
        try {
            if (!size || !stuffing) {
                throw new HamburgerException('MissingData', "no size or stuffing given");
            }
            if (size.paramType != "Size" || stuffing.paramType != "Stuffing") {
                throw new HamburgerException('UncorectedParameters', "invalid size or stuffing");
            }
            this.size = size;
            this.stuffing = stuffing;
        } catch (err) {
            alert(err.name + ": " + err.message);
        }
    }
    addTopping(topping) {

        try {
            if (!topping) {
                throw new HamburgerException('MissingData', "no topping given");
            } else if (topping.paramType != "Topping") {
                throw new HamburgerException('UncorectedParameters', "invalid topping");
            }
            if (topping.added === true) {
                throw new HamburgerException('DuplicateTopping', `Topping ${topping.paramName} has been already added`);
            }

            if (topping.paramName == "Mayo") {
                this.toppingMayo = topping;
                topping.added = true;
            } else {
                this.toppingSpice = topping;
                topping.added = true;
            }

        } catch (err) {
            alert(err.name + ": " + err.message);
        }
    }
    removeTopping(topping) {
        try {
            if (!topping) {
                throw new HamburgerException('MissingData', "no topping given");
            } else if (topping.type != "Topping") {
                throw new HamburgerException('UncorectedParameters', "invalid topping");
            }
            if (topping.added === false) {
                throw new HamburgerException('MissingTopping', `Topping ${topping.paramName} was not added`);
            }

            if (topping.paramName == "Mayo") {
                delete this.toppingMayo;
                topping.added = false;
            } else {
                delete this.toppingSpice;
                topping.added = false;
            }

        } catch (err) {
            alert(err.name + ": " + err.message);
        }
    }
    getToppings() {
        var addedTopping = new Array(), i = 0;
        for (let prop in this) {
            if (this[prop].added === true) {
                addedTopping[i] = this[prop].paramName;
                i++;
            }
        }
        return addedTopping;
    }
    getSize() {
        return this.size.paramName;
    }
    getStuffing() {
        return this.stuffing.paramName;
    }
    calculatePrice() {
        let price = 0;
        for (let prop in this) {
            if (this[prop].price) {
                price = this[prop].price + price;
            }
        }
        pPrice.innerHTML = price;
        return price;
    }
    calculateCalories() {
        let calories = 0;
        for (let prop in this) {
            if (this[prop].calories) {
                calories = this[prop].calories + calories;
            }
        }
        pCalories.innerHTML = calories;
        return calories;
    }
}

class Parameters {
    constructor(type, name, price, calories) {
        this.paramType = type;
        this.paramName = name;
        this.price = price;
        this.calories = calories;
    }
}

Hamburger.SIZE_SMALL = new Parameters("Size", "Small", 50, 20);
Hamburger.SIZE_LARGE = new Parameters("Size", "Large", 100, 40);
Hamburger.STUFFING_CHEESE = new Parameters("Stuffing", "Cheese", 10, 20);
Hamburger.STUFFING_SALAD = new Parameters("Stuffing", "Salad", 20, 5);
Hamburger.STUFFING_POTATO = new Parameters("Stuffing", "Potato", 15, 10);
Hamburger.TOPPING_MAYO = new Parameters("Topping", "Mayo", 20, 5);
Hamburger.TOPPING_MAYO.added = false;
Hamburger.TOPPING_SPICE = new Parameters("Topping", "Spice", 15, 0);
Hamburger.TOPPING_SPICE.added = false;

class HamburgerException {
    constructor(name, message) {
        this.name = name;
        this.message = message;
    }
}

const buttonCalculate = document.querySelector("#calculate");
buttonCalculate.addEventListener('click', (ev) => {
    let inputSize, inputStuffing, hamburgerSize, hamburgerStuffng;
    inputSize = document.querySelectorAll('[name="size"]');
    inputSize.forEach((el) => {
        if (el.checked == true) {
            if (el.value == "small") {
                hamburgerSize = Hamburger.SIZE_SMALL
            } else if (el.value == "large") {
                hamburgerSize = Hamburger.SIZE_LARGE
            }
        }
    })
    inputStuffing = document.querySelectorAll('[name="stuffing"]');
    inputStuffing.forEach((el) => {
        if (el.checked == true) {
            switch (el.value) {
                case "cheese": {
                    hamburgerStuffng = Hamburger.STUFFING_CHEESE
                } break;
                case "salad": {
                    hamburgerStuffng = Hamburger.STUFFING_SALAD
                } break;
                case "potato": {
                    hamburgerStuffng = Hamburger.STUFFING_POTATO
                }
            }
        }
    })

    let hamburger = new Hamburger(hamburgerSize, hamburgerStuffng); //Запускаэмо фукцію конструктор та передаємо значення
    Hamburger.TOPPING_MAYO.added = false;
    Hamburger.TOPPING_SPICE.added = false;
    pToppings.innerHTML="";
    pGetSize.innerHTML ="";
    pGetStuffing.innerHTML ="";
    
    if (checkboxMayo.checked == true && !hamburger.toppingMayo) {
        hamburger.addTopping(Hamburger.TOPPING_MAYO)
    }

    if (checkboxSpice.checked == true && !hamburger.toppingSpice) {
        hamburger.addTopping(Hamburger.TOPPING_SPICE)
    }

    hamburger.calculatePrice()
    hamburger.calculateCalories()

    const buttonAllTopping = document.querySelector("#all_toppings")
    buttonAllTopping.addEventListener('click', () => {
        pToppings.innerHTML = hamburger.getToppings().join(", ");
    })

    const buttonGetSize = document.querySelector("#get_size");
    buttonGetSize.addEventListener('click', () => {
        pGetSize.innerHTML = hamburger.getSize();
    })
    
    const buttonGetStuffing = document.querySelector("#get_stuffing");
    buttonGetStuffing.addEventListener('click', () => {
        pGetStuffing.innerHTML = hamburger.getStuffing();
    })
    
})
