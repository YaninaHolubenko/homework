// /*Написать реализацию игры ["Сапер"](http://minesweeper.odd.su/).

// #### Технические требования:
// - Нарисовать на экране поле 8*8 (можно использовать таблицу или набор блоков).
// - Сгенерировать на поле случайным образом 10 мин. Пользователь не видит где они находятся.
// - Клик левой кнопкой по ячейке поля "открывает" ее содержимое пользователю. 
//   - Если в данной ячейке находится мина, игрок проиграл. В таком случае показать все остальные мины на поле. Другие действия стают недоступны, можно только начать новую игру. 
//   - Если мины нет, показать цифру - сколько мин находится рядом с этой ячейкой.
//   - Если ячейка пустая (рядом с ней нет ни одной мины) - необходимо открыть все соседние ячейки с цифрами.
// - Клик правой кнопки мыши устанавливает или снимает с "закрытой" ячейки флажок мины.
// - После первого хода над полем должна появляться кнопка "Начать игру заново",  которая будет обнулять предыдущий результат прохождения и заново инициализировать поле.
// - Над полем должно показываться количество расставленных флажков, и общее количество мин (например `7 / 10`).

// #### Не обязательное задание продвинутой сложности:
// - При двойном клике на ячейку с цифрой - если вокруг нее установлено такое же количество флажков, что указано на цифре этой ячейки, открывать все соседние ячейки.
// - Добавить возможность пользователю самостоятельно указывать размер поля. Количество мин на поле можно считать по формуле `Количество мин = количество ячеек / 6`.*/


window.onload = () => {
    
    startGame(8, 8, 10);
    //Запуск гри
    function startGame(width, height, bombs_count) {
        //Створюємо нове поле
        const field = document.querySelector('.field');
        const cellsCount = width * height;
        field.innerHTML = '<button></button>'.repeat(cellsCount);
        const cells = [...field.children];
        let closedCount = cellsCount;
        //Лічильник прапорів
        let fc = 0;
        //Створюємо міни та розміщуємо у рандомному порядку
        const bombs = [...Array(cellsCount).keys()]
            .sort(() => Math.random() - 0.5)
            .slice(0, bombs_count);
        //Клік лівою кнопкою 
        field.addEventListener('click', (event) => {
            //Якщо кнопка не реагує
            if (event.target.tagName !== 'BUTTON') {
                return;
            }
            //Нова гра
            newGame();
            //Знаходимо індекс кнопки на яку було натиснуто
            const index = cells.indexOf(event.target);
            const column = index % width;
            const row = Math.floor(index / width);
            open(row, column);
        });
        //Створюємо прапори по натисканню на праву кнопку
        field.addEventListener("contextmenu", (e) => {
            e.preventDefault();//Не зрозуміла
            const el = e.target;
            //Запускаємо нову гру
            newGame();
            // Якщо кнопка не реагує
            if (e.target.tagName !== 'BUTTON') {
                return;
            }
            //Якщо кнопку вже натиснуто
            if (el.disabled === true) { return; };
            //Якщо прапор вже був встановлений - прибираємо
            if (el.classList.contains("flag")) {
                el.classList.remove("flag"); 
                fc--;
            } 
            // В інших випадках ставимо прапор
            else { 
                el.classList.add("flag");
                fc++;
            }
            // Створюємо лічильник прапорів та мін         
            const schet = document.querySelector(".flag_count");
            schet.innerHTML = `Флаги: ${fc} / Бомбы: ${bombs_count}`;
            e.stopPropagation();
        });
        //Перевіряємо
        function isValid(row, column) {//Не зрозуміла навіщо ця перевірка?
            return row >= 0
                && row < height
                && column >= 0
                && column < width;
        }
        //Перевіряємо кнопки поряд на наявність мін
        function getCount(row, column) {
            let count = 0;
            for (let x = -1; x <= 1; x++) {
                for (let y = -1; y <= 1; y++) {
                    if (isBomb(row + y, column + x)) {
                        count++;
                    }
                }
            }
            return count;
        }
        //Клік на ліву кнопку мишки
        function open(row, column) {
    
            if (!isValid(row, column)) return;
            //Знаходимо індекс
            const index = row * width + column;
            const cell = cells[index];
            //Якщо вже натиснуто або встановлено прапор - не реагуємо
            if (cell.disabled === true) return;
            if (cell.classList.contains("flag") === true) return;
            //Кнопку натиснута та більше не реагує на кліки
            cell.disabled = true;
            //Якщо встановлено міну
            if (isBomb(row, column)) {
                cell.classList.add("bomb");
                cells.forEach(e => {
                    //Шукаємо усі міни та диактивуємо усі кнопки
                    let ind = cells.indexOf(e);
                    cells[ind].disabled = true;
                    if (bombs.includes(ind)) {
                        cells[ind].classList.add("bomb");
                    }
                })
                alert('Нажаль Ви програли!');
                return;
            };
            //Зменшуємо кількість ненажатих кнопок
            closedCount--;
            //Якщо кількість відкритих клітин меньше чи дорівнює кількості мін, Ви виграли.
            if (closedCount <= bombs_count) {
                alert('Ви виграли!');
            }
            //Відкриваємо кнопки поряд якщо кнопка не = 0
            const count = getCount(row, column);
            if (count !== 0) {
                cell.innerHTML = count;
                return;
            }
            //Відкриваємо кнопки поряд
            for (let x = -1; x <= 1; x++) {
                for (let y = -1; y <= 1; y++) {
                    open(row + y, column + x);
                }
            }
        };
        //Функція перевікки кнопки на наявність міни
        function isBomb(row, column) {
            if (!isValid(row, column)) return false;
            const index = row * width + column;
    
            return bombs.includes(index);
        };
    };
    //Функція нова гра
    function newGame() {
        const newG = document.querySelector(".newGame");;
        newG.innerHTML = 'Нова гра';
        newG.addEventListener('click', (event) => {
            location.reload();
        });
    }
    }
    
    
    

