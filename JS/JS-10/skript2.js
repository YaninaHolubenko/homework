/*Задание 2) При загрузке страницы - показать на ней кнопку с текстом “Нарисовать круг”. 
Данная кнопка должна являться единственным контентом в теле HTML документа, весь остальной контент должен быть создан и добавлен 
на страницу с помощью Javascript. - При нажатии на кнопку “Нарисовать круг” показывать одно поле ввода - диаметр круга. 
При нажатии на кнопку “Нарисовать” создать на странице 100 кругов (10*10) случайного цвета. 
При клике на конкретный круг - этот круг должен исчезать, при этом пустое место заполняться, то есть все остальные круги сдвигаются влево.
 - У вас может возникнуть желание поставить обработчик события на каждый круг для его исчезновения. Это неэффективно,
так делать не нужно. На всю страницу должен быть только один обработчик событий, который будет это делать.*/


    
const btn = document.querySelector('#btn');
  
const container = document.createElement('div');
container.style.cssText = `display: flex; flex-wrap: wrap;`;
  
const input = document.createElement('input');
input.setAttribute('type','text');
input.setAttribute('placeholder','Діаметр круга');
input.setAttribute('value','');
    
  
const btnDraw = document.createElement('button');
btnDraw.innerHTML = "Намалювати";
  
let d;
  
btn.addEventListener('click',() =>{
btn.remove();
document.body.append(input);
document.body.append(btnDraw);
document.body.append(container);
});
  
btnDraw.addEventListener('click', () =>{
    if (input.value === "" || isNaN(+input.value)) {
        d = '10px';
        }
    else {
        d =`${input.value}px`
        };
        input.remove();
        btnDraw.remove();
        for (let i = 0; i < 100; i++) {
          drawingCircle(d)
        }
    });
function drawingCircle(x) {
    const circ = document.createElement('div');
    circ.classList.add('.newCirc');
    let ranNumber = (Math.floor(Math.random()*361));
    circ.style.cssText = `width: ${x};
    height: ${x};
    background-color: hsl(${ranNumber}, 100%, 70%);
    border-radius:50%;
    margin:5px`
    container.append(circ)
    };
  
    container.addEventListener('click',(element) =>{
        //let target = element.target.closest('.newCirc');
        if(element.target.classList.contains('.newCirc')){
            element.target.remove(); 
        }else{
            return
        } 
})