import React from 'react';
import ReactDOM from 'react-dom/client';
import Main from './components/main';


const main = ReactDOM.createRoot(document.querySelector('main'));

main.render(
    <Main/>
);

