import React, { Component } from "react";
import './main.css';
import Section from "../section";
import { BrowserRouter,Route, Routes } from "react-router-dom";
import Header from '../header'



class Main extends Component {
    render() {

        //Перший спосіб створення запиту
        /*   let promise = fetch('https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json');
          promise.then((data)=>{ return data.json()})
          .then((data1)=>{console.log(data1)}); */


        return (
            <>
                <BrowserRouter>
                    <Header />
                    <Routes>
                        <Route path='/currency' element={<Section url='https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json'/>} />
                        <Route path='/bank-info' element={<Section url='https://bank.gov.ua/NBUStatService/v1/statdirectory/basindbank?date=20160101&period=m&json'/>} />
                    </Routes>
                </BrowserRouter>
            </>
        )
    }

}
export default Main;