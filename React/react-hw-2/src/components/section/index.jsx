import React, { Component } from "react";
import { data } from '../reques';
import CircularProgress from '@mui/joy/CircularProgress';
import './section.css'

export default class Section extends Component {
    state = { allCurency: <CircularProgress/>}
    componentDidMount() {
        data(this.props.url)
            .then((data1) => {
                this.setState(({allCurency}) => {
                    return {
                        allCurency: data1
                    }// повертаємо новий об'єкт
                })
            });

    }
    render() {
        const {allCurency} = this.state;
        return (
            <div className="section">
                <table>
                    <tbody>
                        <tr>
                            <th>Currency name</th>
                            <th>Price</th>
                            <th>Cod</th>
                        </tr>
                        {Array.isArray(allCurency)?
                        allCurency.map((item, idex)=>{
                            return(<tr key={idex+'t'}>
                                <td>{item.txt}</td>
                                <td>{item.rate ? item.rate.toFixed(2) : item.value}</td> 
                                <td>{item.cc}</td>
                            </tr>)
                        }):null}
                    </tbody>
                </table>
            </div>)
    }

}

