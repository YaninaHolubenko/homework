import React, { useState, useEffect } from 'react';

import Header from '../header';
import RandomPlanet from '../random-planet';
import ItemList from '../item-list';
import PersonDetails from '../person-details';

import './app.css';
import Service from '../../services/service';
import { BrowserRouter, Route, Routes } from "react-router-dom";


const App = () => {
  return (
    <div>
      <Header />
      <RandomPlanet />
      <div className="row mb2">
        <BrowserRouter>
          <Routes>
            <Route path='/people' className="col-md-6" element={<ItemList request = {new Service().getPeoples()}/>}/>
            <Route path='/planets' className="col-md-6" element={<ItemList request = {new Service().getPlanets()}/>}/>
            <Route path='/starships' className="col-md-6" element={<ItemList request = {new Service().getStarships()}/>}/>
          </Routes>
        </BrowserRouter>

        <div className="col-md-6">
          <PersonDetails />
        </div>
      </div>
    </div>
  );
};

export default App;