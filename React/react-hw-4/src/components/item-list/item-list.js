import React, { useState, useEffect } from 'react';
import CircularProgress from '@mui/material/CircularProgress';
import './item-list.css';
import Service from '../../services/service'
/* new Service().getPeoples().then((date)=>{
  console.log(date)
}) */
const ItemList = ({ request }) => {
  const [property, setProperty] = useState(null);
  useEffect(()=>{
    request.then((date)=>{
      setProperty(date.results)
    })
  },[])
 
  return (
    <ul className="item-list list-group">
    {property === null ? <CircularProgress/> : property.map((item, index)=>{
   return <li className ="list-group-item" key={index*3 +'r'}>{item.name}</li>
    })}
    </ul>
  );
}
export default ItemList;


