import React from 'react';

import './links.css';

const Links = (props) => {
    return(
        <a href={props.link}>{props.text}</a>
    )
}

export default Links;
