import React from 'react';

import Topnavbar from '../top-navbar';
import Img from '../img';
import Texttitle from '../text-title';
import Textblock from '../text-block';
import Button from '../button';
import Footer from '../footer';

import './main.css';





const Main = ({data}) => {
    return(
        <div className='main'>
            <Topnavbar />
            <Img/>
            <div className='fon'>
                <Texttitle text='Rappresent your life with a simple photo' />
                <Textblock text={data.text1}/>
                <Textblock text={data.text2}/>
                
                <Button value='Get started'/>
            </div>
            <Footer text='Copyright by phototime - all right reserved' />
            
        </div>
    )
}

export default Main;

