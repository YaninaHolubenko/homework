import React from 'react';

import './img.css';
import topimg from './imgs/topimg.png';

const Img = () => {
    return(
        <img src={topimg} alt="BigPhoto" />
    )
}

export default Img;
