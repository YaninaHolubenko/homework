import React from 'react';

import './text-block.css';

const Textblock = (props) => {
    return(
        <div className='textblock'>
            {props.text}
        </div>
    )
}

export default Textblock;
