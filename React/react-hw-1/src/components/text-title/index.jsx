import React from 'react';

import './text-title.css';

const Texttitle = (props) => {
    return(
        <p className='texttitle'>
            {props.text}
        </p>
    )
}

export default Texttitle;
