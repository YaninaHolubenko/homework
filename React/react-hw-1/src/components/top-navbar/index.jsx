import React from 'react';
import Links from '../links';

import './topnavbar.css';

const Topnavbar = () => {
    return(
        <div className='navbar'>
            <Links link='#' text='home'/>
            <Links link='#' text='photoapp'/>
            <Links link='#' text='design'/>
            <Links link='#' text='download'/>
        </div>
    )
}

export default Topnavbar;
