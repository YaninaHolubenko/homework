import React from 'react';

import './button.css';

const Button = (props) => {
    return(
        <div className='buttonholder'>
            <input type='button' value={props.value}></input>
        </div>
    )
}

export default Button;
